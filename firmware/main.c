#include <msp430.h>
#include <stdarg.h>  // va_list() magic
#include <stdbool.h> // bool
#include <stddef.h>  // size_t
#include <stdint.h>  // uint8_t
#include <stdio.h>   // vsnprintf
#include <string.h>  // for memset

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define LCD_LINELEN 16
#define LCD_DELAY_SHORT 400
#define LCD_DELAY_LONG 4000
#define UART_BUF_LEN 32
#define ADC10_TO_HV_V(x) (((x * 60) >> 8) * 25) // this is exact: 1024 -> 6000V
#define ADC10_TO_HV_PERCENT(x) ((x * 25) >> 8)  // this is exact: 1024 -> 100%
#define ADC10_TO_MV(x) ((x * 39) >> 4) 	// 39/16 is 2.437, our real factor is 2.441, so thats an error of 0.16%

// Data-sheets
// Controller: https://www.lcd-module.com/eng/pdf/zubehoer/st7036.pdf
// Module: https://www.lcd-module.de/pdf/doma/dog-m.pdf
// This initialization was taken from the module data-sheet

const uint8_t LCD_INIT_CMD[] = { 0x39, 0x13, 0x55, 0x6D, 0x78, 0x38, 0x0C, 0x06,
		0x01 };

const bool LCD_MODE_DATA = true;
const bool LCD_MODE_CMD = false;
const uint8_t ADC10_IIR_BITS = 3;

const uint16_t ENCODER_MAX_VAL = 750;
const uint16_t ENCODER_INCREMENT_BIG = 25;
const uint16_t ENCODER_INCREMENT_SMOL = 1;

volatile int16_t encoder_cnt = 25;
volatile uint8_t mode = 0;
volatile uint16_t adc10_result[2];
const char* mode_str[] = {"OFF", " ON", "ERR"};

char uart_buf[UART_BUF_LEN];
volatile uint8_t uart_buf_pos = 0;
volatile uint8_t uart_buf_cnt = 0;

//               line len:  01234567890123456
const char* startup_msg0 = "HV 6kV, 0.83mA";
const char* startup_msg1 = "Version 20220919";
const uint16_t startup_delay = 6000;

//
//
//

static void delay_ms(uint16_t ms) {
	while (ms > 0) {
		// for each millisecond we need to burn 4k cycles
		uint16_t cnt = 2000;
		while (cnt-- > 0) {
			_nop();
		}
		ms--;
	}
}

static void delay_lcd(uint16_t cnt) {
	while (cnt-- > 0) {
		_nop();
	}
}

/**
 *
 */
void spi_init() {
	// configure pin muxes: Pin7 (MOSI) and Pin5 (SCK) on Port1
	P1SEL |= BIT7 | BIT5;
	P1SEL2 |= BIT7 | BIT5;
	// Port2, Pins 6 and 7 are used as control signals
	P2DIR |= (BIT6 | BIT7);
	P2OUT |= (BIT6 | BIT7); // de-assert CS

	// set the SWRST bit
	UCB0CTL1 |= UCSWRST;

	// Set SPI Mode
	// The display does sample on the rising CLK edge
	//          SPI    | Master | ClockPol | MSB First
	UCB0CTL0 |= UCSYNC | UCMST | UCCKPL | UCMSB;

	// Use SMCLK as clock source
	UCB0CTL1 |= UCSSEL_2;
	// Set clock divider to 4, that makes one bit 4us long, so a whole bytes
	// takes 32us, which is well above the 23us time required by the display
	// to execute a command
	UCB0BR0 = 0x08;
	UCB0BR1 = 0x00;
	// Release reset to enable the peripheral
	UCB0CTL1 &= ~UCSWRST;
}

void spi_send_bytes(const uint8_t *bytes, size_t len, bool rs) {
	if (len == 0) {
		// cannot send zero sized data
		return;
	}
	// RS is connected to P2.6
	// RS = 1 means we are writing data, otherwise instructions are written
	if (rs) {
		P2OUT |= BIT6;
	} else {
		P2OUT &= ~BIT6;
	}
	delay_lcd(LCD_DELAY_SHORT);
	// de-assert  CS line
	P2OUT &= ~BIT7;
	delay_lcd(LCD_DELAY_SHORT);
	// write a bunch of bytes
	for (; len > 0; len--) {
		while (UCB0STAT & UCBUSY) {
		};
		UCB0TXBUF = *bytes++;
		delay_lcd(LCD_DELAY_LONG);
	}
	// wait for the transfer to finish
	while (UCB0STAT & UCBUSY) {
	};
	delay_lcd(LCD_DELAY_SHORT);
	// release CS line
	P2OUT |= BIT7;

	// debug/test: wait 1ms after each command
	delay_lcd(LCD_DELAY_SHORT);
}

void lcd_write(uint8_t address, const char *format, ...) {
	va_list args;
	va_start(args, format);

	// send a DRAM address to the display
	uint8_t cmd = 0x80 + MIN(0x79, address);
	spi_send_bytes(&cmd, 1, LCD_MODE_CMD);

	// print the string to our buffer
	uint8_t glcd_bytes[LCD_LINELEN + 1]; // we need one more for char the null terminator

	uint8_t end = vsnprintf((char*) glcd_bytes, sizeof(glcd_bytes), format,
			args);
	if (end < sizeof(glcd_bytes)) {
		// pad the memory with whitespace chars (0x20)
		memset(&glcd_bytes[end], 0x20, sizeof(glcd_bytes) - end);
	}
	spi_send_bytes(glcd_bytes, sizeof(glcd_bytes) - 1, LCD_MODE_DATA); // always send 16 chars

	va_end(args);
}

// Warning: this is not thread-safe, don't call it from an ISR
void uart_write(const char *format, ...) {
	va_list args;
	va_start(args, format);
	// print the string to our buffer

	while (uart_buf_cnt != 0) {
		// wait until the buffer is empty again, we don't want to overwrite stuff
		// ideally we would create some kind of ring-buffer. That exercise is left for the reader
		_nop();
	}

	uint8_t cnt = vsnprintf((char*) uart_buf, sizeof(uart_buf), format,
			args);
	uart_buf_cnt = MIN(cnt, sizeof(uart_buf));
	va_end(args);

	// send the first byte
	uart_buf_pos = 0;
	UCA0TXBUF = uart_buf[uart_buf_pos++];
}


void quadrature_init() {
	P2IFG = 0;
	// we only want to trigger on one of the quadrature-encoder pins
	// so enable interrupts for 'A' and 'Switch'
	P2IE |= BIT0 | BIT1; // enable interrupts for P2.0 P2.1
	P2IES |= BIT0 | BIT1; // trigger on High->Low Transition
}

void uart_init() {
	   P1SEL |= BIT1 | BIT2 ; // P1.1 = RXD, P1.2=TXD
	   P1SEL2 |= BIT1 | BIT2; // P1.1 = RXD, P1.2=TXD

	   UCA0CTL1 |= UCSSEL_2; // SMCLK
	   UCA0BR0 = 0x10; // 1MHz 115200 !!! 0x08 should be correct for 1MHz, it seems out clock is 2MHz?
	   UCA0BR1 = 0x00; // 1MHz 115200
	   UCA0MCTL = UCBRS2 + UCBRS0; // Modulation UCBRSx = 5
	   UCA0CTL1 &= ~UCSWRST; // Initialize USCI state machine
	   UC0IE |= UCA0RXIE | UCA0TXIE; // Enable USCI_A0 RX and TX interrupt
}

void analog_init() {
    ADC10AE0 = BIT3 | BIT4; // Analog inputs on P1.3 and P1.4

    ADC10CTL0 = ADC10ON| REFON | REF2_5V | SREF_1| ADC10SHT_3 | ADC10SR;
    ADC10CTL1 = CONSEQ_0 | ADC10SSEL_3 | ADC10DIV_2 | INCH_3;

    ADC10CTL0 |= ADC10IE;   // Enable ISR
    ADC10CTL0 |= ENC | ADC10SC;
}

void startup_message() {
	// LED-Test
	P2OUT |= BIT3 | BIT4 | BIT5;

	// split up the UART message, with the LCD write inbetween, so the UART has time
	// to clear the initial buffer
	uart_write(startup_msg0);
	lcd_write(0x00, startup_msg0);
	lcd_write(0x40, startup_msg1);
	uart_write(startup_msg1);

	// keep this message for a few seconds
	delay_ms(startup_delay);

	P2OUT &= ~(BIT3 | BIT4 | BIT5);
}

/**
 * main.c
 */
int main(void) {
	// disable the WDT
	WDTCTL = WDTPW | WDTHOLD;

	// set some clocking (taken from another project)
	DCOCTL = 0b10000000;    // 4 MHz
	BCSCTL1 = 0b10001111;   // 4 MHz, no XTAL
	BCSCTL2 = 0b00000110;   // SMCLK = DCO/8 = 1 MHz

	// initialize unused ports
	P2DIR = 0;
	P2OUT = 0;
	P3DIR = 0;
	P3OUT = 0;

	P1SEL = 0; // all ports as GPIO (we need this since some are configured for XTAL)
	P2SEL = 0;

	// LEDs on P2.3 .. P2.5 as output
	P2DIR |= BIT3 | BIT4| BIT5;
	P2OUT ^= BIT5;

	// configure Timer1 as tick-timer
	TA1CCTL0 = CCIE;  // CCR0 interrupt enabled
	TA1CTL = TASSEL_2 + MC_1 + ID_3;  // SMCLK/8, up-counting-mode
	TA1CCR0 = 12500;  // Set CaptureCompareRegister so we get an 10Hz Interrupt

    // Configure Timer A0 interrupt
    TA0CTL = TASSEL_2 + ID_0 + MC_1; // Timer_A1: SMCLK clock, input divider=1, "up" mode
    TA0CCR0 = 1000;                  // Set Timer_A1 period to 1ms for 1kHz PWM
    TA0CCR1 = 0;                     // start with 0 as duty cycle
    TA0CCTL1 = OUTMOD_7;             // Select "Reset/Set" output mode

    // Configure PWM output
    P1SEL |= BIT6;
    P1SEL2 &= ~BIT6;
    P1DIR |= BIT6;

    // Enable interrupt for the HV-On switch
	P1IE |= BIT0; // enable interrupts for P1.0
	P1IES = 0; // trigger on Low->High Transition


	// configure interrupts for the rotary encoder
	quadrature_init();
	uart_init();
	analog_init();
	_BIS_SR(GIE);

	// configure the LCD
	spi_init();
	delay_ms(100);
	spi_send_bytes(LCD_INIT_CMD, sizeof(LCD_INIT_CMD), LCD_MODE_CMD);

	startup_message();

	// a stupid main loop that displays some stuff, the sleeps
	while (1) {
		// forward encoder-count to PWM-Out
		// we can do this since the encoder_cnt is limited within the ISR to its min/max values
		TA0CCR1 = encoder_cnt;

		// ADC stuff
		uint16_t load = ADC10_TO_HV_PERCENT(adc10_result[0]);
		uint16_t voltage = ADC10_TO_HV_V(adc10_result[1]);
		uart_write("%d, %d, %d, %d\r\n", mode, load, voltage, encoder_cnt*8);

		// Update the UI elements
		// write stuff to the LCD
		lcd_write(0x00, "Set: %4dV   %s", encoder_cnt*8, mode_str[mode]);
		lcd_write(0x40, "Is: %4dV @ %3d%%", voltage, load);
		// use the red LED to show that a overload is imminent
		if (load > 90) {
			P2OUT |= BIT5;
		} else {
			P2OUT &= ~BIT5;
		}
		// use the orange LED to show that HV is on
		if (mode > 0) {
			P2OUT |= BIT4;
		} else {
			P2OUT &= ~BIT4;
		}

        // Go back to sleep, Enter LPM0 w/ interrupt
		_BIS_SR(CPUOFF + GIE);
	}
}


#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR (void)
{
	uint8_t idx;
	uint16_t tmp;
	// delete int flag, disable conversion
	ADC10CTL0 &= ~(ADC10IFG | ENC);

	// switcheroo channels
	if ((ADC10CTL1 & 0xF000) >> 12  == 3) {
		// channel 3 is selected, switch to 4
		ADC10CTL1 &= 0x0FFF;
		ADC10CTL1 |= INCH_4;
		idx = 0;
	} else {
		ADC10CTL1 &= 0x0FFF;
		ADC10CTL1 |= INCH_3;
		idx = 1;
	}


    // get result, use IIR filter;
	// we can use this up to a *64 filter, then the uint16_t will overflow
	tmp = adc10_result[idx] * ((1 << ADC10_IIR_BITS) - 1) + ADC10MEM;
    adc10_result[idx] = tmp >> ADC10_IIR_BITS;
    // trigger next conversion
    ADC10CTL0 |= ADC10SC | ENC;
}


#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
	// remove the flag
	IFG2 &= ~UCA0TXIFG;
	// we use _cnt == 0 as indicator that the buffer is empty
	if (uart_buf_cnt != 0) {
		UCA0TXBUF = uart_buf[uart_buf_pos++];

		if (uart_buf_pos == uart_buf_cnt) {
			// we just send the last byte
			uart_buf_cnt = 0;
		}
	}
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)  {
	char tmp = UCA0RXBUF;
}


#pragma vector=PORT1_VECTOR
__interrupt void Port1_ISR(void) {
	P1IFG &= ~BIT0; // remove interrupt flag

	if (P1IN & BIT0) {
		mode = 1;
		P1IES = BIT0;
	} else {
		mode = 0;
		P1IES = 0;
	}
}


#pragma vector=PORT2_VECTOR
__interrupt void Port2_ISR(void) {
	static uint16_t encoder_increment = 1;
	bool A = P2IN & BIT0;
	bool B = P2IN & BIT2;

	if (P2IFG & BIT1) {
		// the button was pressed
		P2IFG &= ~BIT1; // remove interrupt flag
		UCA0TXBUF = 'x';
		if (encoder_increment == ENCODER_INCREMENT_SMOL) {
			encoder_increment = ENCODER_INCREMENT_BIG;
		} else {
			encoder_increment = ENCODER_INCREMENT_SMOL;
		}
	} else if (P2IFG & BIT0) {
		// Not a button press, but encoder action
		if (A == B) {
			encoder_cnt += encoder_increment;
			if (encoder_cnt > ENCODER_MAX_VAL) {
				encoder_cnt = ENCODER_MAX_VAL;
			}
		} else {
			encoder_cnt -= encoder_increment;
			if (encoder_cnt < 0) {
			  encoder_cnt = 0;
			}
		}

		// now switch interrupt edge
		if (P2IES & BIT0) {
			P2IES &= ~(BIT0);
		} else {
			P2IES |= BIT0; // trigger on High->Low Transition
		}
		P2IFG &= ~(BIT0); // remove interrupt flags
	} else {
		_nop();
	}
	__low_power_mode_off_on_exit();
}


// Timer A1 interrupt service routine
#pragma vector=TIMER1_A0_VECTOR
__interrupt void Timer_A(void) {
	static uint8_t divider = 0;



	// only exit sleep every 10th time
	if (++divider == 10) {
		P2OUT ^= BIT3;
		divider = 0;
	}
	__low_power_mode_off_on_exit();
}
