HighVoltage Supply :zap:
===
>
> :warning: Only use this device if you know that you are doing!
>
> The ground connection is **NOT ISOLATED** from the HV ground.
>
> :zap: High Voltage can kill you!

![Preview Rendering](./exports/Case_3DModel.png)

# Overview
* The device is build around the `HRC0524S6K0N` module from XP-Power. Thus its output voltage is `0 .. -6000V` with a maximum current of `0.83mA`.
* The target-voltage can be controlled using the rotaty-encoder. It has a coarse and fine mode (200V, 8V) that can be toggled by pressing the encoder button. There is no feedback on which mode is active.
* A `Enable`-switch can be used to switch the output on or off.
* The LC-Display shows the set target voltage, the position of the enable-switch, the measured voltage and output-load
* The output terminals are 4mm jacks spaced 19mm
* There is an 2nd output on the back with a SHV Jack (`TE 51494-2`)
* A external power-supply with a 5.5/2.1mm Barrel-Jack with 24V and a minimum of 500mA is requited.

```mermaid
graph TB
    A{HV Module} -->L([HV Output Jack])
    A -->|Feedback| B(OpAmp) 
    J(OpAmp) --> |Setpoint|A
    C{MSP430 MCU} --> J(OpAmp)
    B --> C{MSP430 MCU}
    K([DC 24V Jack]) --> A
    K --> R[3.3V Regulator]
    R --> |3V3| C
    E[Rotary Encoder]  -->  C
    I[Enable Switch]  -->  A
    C <-->|UART| F[OptoIsolator]
    F <--> U[USB-Serial]
    C -->|SPI| H[LC Display]
```

# Measurement accuracy ⚖️
Because of the poor analog design[^analog], the measured voltage is not very accurate. Two LSBs of the ADC10 noise already result in a 50V error.
The measurement of the current-load is also not very good, especially in the lower ranges below 50% load, there is lots of oscillation noise that impacts the result.
Since we also want reasonable fast response-times, adding more low-pass filtering is not an option.

[^analog]: 
    * No proper Reference-Voltage to measure against. The OpAmp uses the 5V Auxilliary output of the HV-Module, the ADC uses the internal 2.5V reference.
    * The HV-Module has an output-impedance of 10k. We need to divide its voltage down by two, using another 10k resistor. Both of those values are not accurate, that introduces quite some error.
    * No pots in the resistor-feedback-path of the OpAmp, thus no calibration is possible
    * The OpAmp is a Rail-2-Rail type, but still cannot go down completely to 0 or 5V
    * The MSP430 does not have hardware multiply or divide instructions. So we have to rely on simple multiplications and bit-shift for math, introducting additional arithmetic errors
    * One LSB 

# Digital Interface :computer: 
The devices features a USB-B port to be connected to a host computer. It enumerates as PL2303 USB-to-serial device. Connect to it with `115200@8N1`[^serial].
Currently no digital control is implemented. The device just outputs the the device state regularly.

[^serial]: That means 115200Baud with 8 Bits, no Parity, one Start-Bit

A typical output looks like this:
```
0, 0, 50, 2400
0, 0, 50, 2400
0, 0, 50, 2400
0, 0, 50, 2400
0, 0, 50, 2400
0, 0, 50, 2400
0, 0, 50, 2400
1, 71, 2300, 2400
1, 73, 2325, 2400
1, 74, 2325, 2400
1, 74, 2325, 2400
1, 74, 2325, 2400
1, 74, 2325, 2400
```

This can be parsed with a regular expression like so
```python
# python example
import re
m = re.compile(r"^(\d+),\s(\d+),\s(\d+),\s(\d+)$")
(onoff, load_precent, voltage_is, voltage_set) = m.match("1, 74, 2325, 2400").groups()
```

# Design Documents :blue_book: 
* Schematic and layout are done in `Target3001!`
* [Schematic](exports/hv-supply-usb_SCH.PDF) exported as .pdf
* Various other exports in the exports directory
* The case used is from Bopla (55320000), its about 160x200x62mm

## Design Process and considerations
* Part selection
  * The Physik IIIA Werkstatt uses MSP430 MCUs, so the design centers around on of those
  * The MSP430G2553 was choosen since its already used in many other designs
  * Most of the parts were selected since they are available in the local stockpile
* Case
  * The case just needs to fit all the components
  * A isolating case is required
* Layout
  * First, all mechanical parts were placed in the Fusion360 mockup
  * Those positions were exported to a 2D `.dxf` file and imported in Target3001!
  * Once the PCB-Layout was done, the 3D-Stepfile export from Target3001! was again imported in Fusion360 to check the alignment. Where required, corrections were made.
* Frontpanel
  * The drawing was exported from Fusion360 and imported on the PCB-Mill
  * It is milled out of 3mm grey plastics (most likely PVC)

## Firmware
There is nothing special about the firmware. Everything is contained in [firmare/main.c](firmware/main.c). The firmware does have some inline comments that explain how exactly it works. Use **CodeComposerStudio** to open the project.

There are lots of warnings of the compiler, mostly centered around optimization (UltraLowPowerAdvice):
* Use of multiplication
* Use of vsnprintf()
* Polling of Flags
* Software Delay loops

All of those are intended. We sure need multiplication and vsnprintf(). The delays and polling could be fixed, but there is no benefit here. We are not power constrained and can spend some cycles waiting.

---

---
